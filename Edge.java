/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hypergraph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

/**
 *
 * @author shill
 */
public class Edge {
    
    private ArrayList<Node> nodes;
    private int weight;
    
    /**
     *
     * @param nodes
     * @param weight
     */
    public Edge(ArrayList nodes, int weight){
        this.nodes = nodes;
        this.weight = weight;
    }

    /**
     *
     * @param nodeA
     * @param nodeB
     * @param weight
     */
    public Edge(Node nodeA, Node nodeB, int weight){
        nodes = new ArrayList();
        this.nodes.add(nodeA);
        this.nodes.add(nodeB);
        this.weight = weight;
    }

    /**
     *
     * @param weight
     */
    public Edge(int weight){
        this.weight = weight;
        nodes = new ArrayList();
    }

    /**
     *
     * @return
     */
    public ArrayList<Node> getNodes() {
        return nodes;
    }

    /**
     *
     * @param nodes
     */
    public void setNodes(ArrayList nodes) {
        this.nodes = nodes;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.nodes);
        hash = 53 * hash + this.weight;
        return hash;
    }

   
    
    public boolean sameNodes(Edge e){
        Collections.sort(this.nodes);
        Collections.sort(e.nodes);
        return Objects.equals(this.nodes,e.nodes);
    }
    
    /**
     *
     * @param node
     */
    public void addNode(Node node){
        nodes.add(node);
    }

    /**
     *
     * @return
     */
    public int getWeight() {
        return weight;
    }

    /**
     *
     * @param weight
     */
    public void setWeight(int weight) {
        this.weight = weight;
    }
    
}
