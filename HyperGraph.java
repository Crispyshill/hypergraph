/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hypergraph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author shill
 */
public class HyperGraph {

    private final ArrayList<Node> nodeList;
    private final ArrayList<Edge> edgeList;
    private static final int MAX_NODES = 20;
    private ArrayList<ArrayList<Integer>> adjList = new ArrayList();

    /**
     * Constructor
     */
    public HyperGraph() {
        nodeList = new ArrayList();
        edgeList = new ArrayList();
    }

    /**
     *
     * @param label
     */
    public void addNode(String label) {
        Node t = new Node(label);
        nodeList.add(t);
        // adjList.add(new int[MAX_NODES]);
        adjList.add(new ArrayList());
        for (ArrayList l : adjList) {
            while (l.size() < nodeList.size()) {
                l.add(0);
            }
        }

    }

    /**
     *
     * @param n
     */
    public void addNode(Node n) {
        nodeList.add(n);
        // adjList.add(new int[MAX_NODES]);
        // adjacencyList.put(n, new ArrayList());.
        adjList.add(new ArrayList());
        for (ArrayList l : adjList) {
            while (l.size() < nodeList.size()) {
                l.add(0);
            }
        }

    }

    /**
     *
     * @param label
     */
    public void removeNode(String label) {
        for (int i = 0; i < nodeList.size(); i++) {
            if (nodeList.get(i).getLabel().equals(label)) {

                for (Edge e : edgeList) {
                    for (Node o : e.getNodes()) {
                        if (o.equals(label)) {
                            e.getNodes().remove(o);
                        }
                    }
                }
                nodeList.remove(i);

                return;
            }
        }
        System.out.println("No node with the label: " + label + " exists!");
    }

    /**
     *
     * @param nodes
     * @param weight
     */
    public void addEdge(int[] nodes, int weight) {
        Edge e = new Edge(weight);
        //System.out.println(adjList.size());
        for (int i = 0; i < nodes.length; i++) {
            e.addNode(nodeList.get(nodes[i]));
            //adjList.set(nodes[i], removeElementFromArray(nodes.clone(), i));

            for (int u = 0; u < nodes.length; u++) {
                adjList.get(nodes[i]).set(nodes[u], adjList.get(nodes[i]).get(nodes[u]) + 1);

            }
            adjList.get(nodes[i]).set(nodes[i], adjList.get(nodes[i]).get(nodes[i]) - 1);

        }

        edgeList.add(e);
    }

    /**
     *
     */
    public void printEdges() {
        for (Edge e : edgeList) {
            System.out.print("Weight: " + e.getWeight() + ", Node: ");
            for (Node n : e.getNodes()) {
                System.out.print(" " + n.getLabel());
            }
            System.out.println("");
        }
    }

    /**
     *
     */
    public void printNodes() {
        for (Node n : nodeList) {
            System.out.println(n.getLabel());
        }
    }

    public void printAdjList() {
        for (int i = 0; i < adjList.size(); i++) {
            System.out.print(i + ": ");
            for (int e : adjList.get(i)) {
                System.out.print(e + " ");
            }
            System.out.println("");
        }
    }

    public ArrayList dimensionReductionShortestPath(int start) {
        HyperGraph flattened = new HyperGraph();
        HashMap<HashSet<Node>, Integer> newEdges = new HashMap();
        for (Node n : nodeList) {
            flattened.addNode(n);
        }
        for (Edge e : edgeList) {
            for (int i = 0; i < e.getNodes().size() - 1; i++) {
                for (int u = i + 1; u < e.getNodes().size(); u++) {
                    HashSet<Node> pair = new HashSet();
                    pair.add(e.getNodes().get(u));
                    pair.add(e.getNodes().get(i));
                    if (newEdges.get(pair) == null) {
                        newEdges.put(pair, e.getWeight());

                    } else if (newEdges.get(pair) > e.getWeight()) {
                        newEdges.put(pair, e.getWeight());
                    }

                }
            }
        }
        for (Map.Entry<HashSet<Node>, Integer> entry : newEdges.entrySet()) {
            if (entry.getKey().isEmpty()) {
                break;
            }
            int[] pos = new int[2];
            List<Node> l = entry.getKey().stream().collect(Collectors.toList());
            //for(Node n : l){
            //}
            //System.out.println("");
            pos[0] = nodeList.indexOf(l.get(0));
            pos[1] = nodeList.indexOf(l.get(1));

            flattened.addEdge(pos, entry.getValue());

        }
        // flattened.printEdges();
        //flattened.printAdjList();
        return flattened.DijkstraAlgorithm(start);

    }

    // runs in O(n^2) where n is the number of edges
    private void condenseEdgesOptimal() {
        for (int i = 0; i < edgeList.size() - 1; i++) {
            for (int j = i + 1; j < edgeList.size(); j++) {
                if (edgeList.get(i).sameNodes(edgeList.get(j))) {
                    if (edgeList.get(i).getWeight() < edgeList.get(j).getWeight()) {
                        edgeList.remove(j);
                    }
                }
            }
        }

    }

    private int[] removeElementFromArray(int[] nums, int index) {
        int temp;
        for (int i = index; i < nums.length - 1; i++) {
            nums[i] = nums[i + 1];
        }
        int[] output = Arrays.copyOf(nums, nums.length - 1);
        return output;
    }
    
   

    public ArrayList DijkstraAlgorithm(int start) {
        HashMap<HashSet<Integer>, Integer> edgeMap = new HashMap();
        //printEdges();
        HashSet t;
        for (Edge e : edgeList) {
            t = new HashSet();
            for (Node n : e.getNodes()) {
                t.add(nodeList.indexOf(n));
            }
            edgeMap.put(t, e.getWeight());
        }

        ArrayList<ArrayList<Integer>> shortestPath = new ArrayList<ArrayList<Integer>>();
        int[] shortestDistance = new int[MAX_NODES];
        shortestDistance[start] = 0;
        while (shortestPath.size() < nodeList.size()) {
            shortestPath.add(new ArrayList<Integer>());
        }
        shortestPath.get(start).add(start);
        nodeList.get(start).setVisited(true);
        HashSet<Integer> pair;
        int tempShortest = Integer.MAX_VALUE;
        int[] indexPair = new int[2];
        int nodesReached = 1;

        while (nodesReached < nodeList.size()) {
            for (int i = 0; i < nodeList.size(); i++) {
                //System.out.println(i);
                if (nodeList.get(i).isVisited()) {
                    //System.out.println("checking adj of v " + i);
                    for (int j = 0; j < adjList.get(i).size(); j++) {
                        // System.out.println("j: " + j);
                        if ((!(nodeList.get(j).isVisited())) && adjList.get(i).get(j) > 0) {
                            //System.out.println("i " + i + " j " + j);

                            // System.out.println("Im in here");
                            pair = new HashSet();
                            pair.add(i);
                            pair.add(j);
                            //System.out.println("the pair is: " + nodeList.get(i).getLabel() + ", " + nodeList.get(j).getLabel());
                            //System.out.println(edgeMap.containsKey(pair));
                            if (edgeMap.get(pair) != null && (edgeMap.get(pair) + shortestDistance[i]) < tempShortest) {
                                //System.out.println("j: " + j);
//                                if(j == 6){
//                                    System.out.println("i: " + i + " j: " + j);
//                                    System.out.println(edgeMap.get(pair));
//                                    System.out.println(shortestDistance[i]);
//                                }
                                // System.out.println("and here");
                                //System.out.println("Pair thing: " + edgeMap.get(pair));
                                tempShortest = edgeMap.get(pair) + shortestDistance[i];
                                indexPair = new int[2];
                                indexPair[0] = i;
                                indexPair[1] = j;
                                //System.out.println("Temp shortest: " + tempShortest);
                            }
                            
                        }
                    }
                }

            }
            //System.out.println("adding " + nodeList.get(indexPair[1]).getLabel());
            pair = new HashSet();
                            //System.out.println("Index pair " + indexPair[0] + " " + indexPair[1]);
                            pair.add(indexPair[0]);
                            pair.add(indexPair[1]);
                            shortestDistance[indexPair[1]] = tempShortest;
                            //System.out.println("index pair 0: " + indexPair[0]);
                            //System.out.println("index pair 1: " + indexPair[1]);
                            //System.out.println(shortestPath.get(indexPair[0]));
                            shortestPath.get(indexPair[1]).addAll(shortestPath.get(indexPair[0]));
                            shortestPath.get(indexPair[1]).add(indexPair[1]);
                            nodesReached++;
                            nodeList.get(indexPair[1]).setVisited(true);
                            //System.out.println("reached: " + indexPair[1]);
                            tempShortest = Integer.MAX_VALUE;

        }
        ArrayList<Path> pathNodes = new ArrayList();
        ArrayList<Node> tempDirection;
        
        for(int i = 0; i < shortestPath.size(); i ++){
            tempDirection = new ArrayList();
            for(int c : shortestPath.get(i)){
                tempDirection.add(nodeList.get(c));
            }
            Path tempPath = new Path();
            tempPath.direction = tempDirection;
            tempPath.length = shortestDistance[i];
            pathNodes.add(tempPath);
        }

        return pathNodes;

    }

}
