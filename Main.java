/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hypergraph;

import java.util.ArrayList;

/**
 *
 * @author shill
 */
public class Main {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {

        HyperGraph graph = new HyperGraph();
        int nodething = 7;

        // Create Nodes
        graph.addNode("A");
        graph.addNode("B");
        graph.addNode("C");
        graph.addNode("D");
        graph.addNode("E");
        graph.addNode("F");
        graph.addNode("G");
        graph.addNode("H");
        graph.addNode("I");
        graph.addNode("J");

        // Create Edges
        graph.addEdge(new int[]{0, 2, 3, 8}, 1);
        graph.addEdge(new int[]{1, 2}, 2);
        graph.addEdge(new int[]{2, 3, 4, 5, 6}, 2);
        graph.addEdge(new int[]{6, 7, 9}, 1);
        graph.addEdge(new int[]{8, 9}, 1);

        //System.out.println("Edge List:");
        //graph.printEdges();
       // System.out.println("Adj List:");
       // graph.printAdjList();
        
        System.out.println("***********************");
        System.out.println("**dimension reduction**");
        System.out.println("***********************");
        
        ArrayList<Path> shortestPath = graph.dimensionReductionShortestPath(0);
        
        System.out.print("The Shortest path from A to H is: " );
        for(Node n : shortestPath.get(nodething).direction){
            System.out.print(n.getLabel() + ", ");
        }
        System.out.println("");
        System.out.println("The distance from A to H is: " + shortestPath.get(nodething).length);

    }
}
