/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hypergraph;

import java.util.Objects;

/**
 *
 * @author shill
 */
public class Node implements Comparable<Node>{

    private String label;
    private boolean visited;

    /**
     *
     * @param label
     */
    public Node(String label) {
        this.label = label;
        this.visited = false;
    }

    /**
     *
     * @return
     */
    public boolean isVisited() {
        return visited;
    }

    /**
     *
     * @param visited
     */
    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    /**
     *
     * @return
     */
    public String getLabel() {
        return label;
    }

    /**
     *
     * @param label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + Objects.hashCode(this.label);
        hash = 19 * hash + (this.visited ? 1 : 0);
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
  

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Node other = (Node) obj;
        if (!Objects.equals(this.label, other.label)) {
            return false;
        }
        return true;

    }

    @Override
    public int compareTo(Node t) {
        return this.label.compareTo(t.label);
    }

}
